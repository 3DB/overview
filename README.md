# Overview

## BigBossBlockchain
The blockchain based backendservice. Includes APIs for communication with other nodes.
[https://gitlab.com/3DB/BigBossBlockchain](https://gitlab.com/3DB/BigBossBlockchain)

## ChargingServer
Backendservice responsible for communication with the app.
[https://gitlab.com/3DB/ChargingServer](https://gitlab.com/3DB/ChargingServer)

## ChargingStation
Responsible for authentication and general communication between the charging station and the user.
[https://gitlab.com/3DB/ChargingStationReal](https://gitlab.com/3DB/ChargingStationReal)

## AncharApp
The android app; map integration, authentication and history of contracts.
[https://gitlab.com/3DB/AncharApp](https://gitlab.com/3DB/AncharApp)

## HoverWare
Hacked hoverboard firmware (modified version of [larsmm's code](https://github.com/larsmm/hoverboard-firmware-hack-bbcar)).
[https://gitlab.com/3DB/HoverWare](https://gitlab.com/3DB/HoverWare)

## HoverControl
Microcontroller code for the I2C communication; responsible for the overall control of the kettcar.
[https://gitlab.com/3DB/HoverControl](https://gitlab.com/3DB/HoverControl)

## ChargingStationCom
Charging control code in the charging station; follows IEC 61851.
[https://gitlab.com/3DB/ChargingStationCom](https://gitlab.com/3DB/ChargingStationCom)

## VerbraucherCom
Charging control code in the kettcar; follows IEC 61851.
[https://gitlab.com/3DB/VerbraucherCom](https://gitlab.com/3DB/VerbraucherCom)

## CopyCharge
Tool for replicating Mifare CLASSIC cards.
[https://gitlab.com/3DB/CopyCharge](https://gitlab.com/3DB/CopyCharge)

## BlockchainVisualizer
Visualizes the blockchain network data in realtime.
[https://gitlab.com/3DB/BlockchainVisualizer](https://gitlab.com/3DB/BlockchainVisualizer)